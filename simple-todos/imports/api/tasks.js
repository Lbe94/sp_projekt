/**
 * Created by Luka on 6. 01. 2017.
 */
import { Mongo } from 'meteor/mongo';

export const Tasks = new Mongo.Collection('tasks');