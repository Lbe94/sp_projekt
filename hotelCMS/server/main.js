import { Meteor } from 'meteor/meteor';
import '../imports/api/tasks.js';
import '../imports/api/kapacitete.js';
import '../imports/api/rezervacije.js';
import '../imports/api/zasedenosti.js';

Meteor.startup(() => {
  // code to run on server at startup
});
