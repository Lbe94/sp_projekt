/**
 * Created by Luka on 8. 01. 2017.
 */
import { Mongo} from 'meteor/mongo';

export const Tasks = new Mongo.Collection('tasks');