/**
 * Created by Luka on 8. 01. 2017.
 */
import { Template} from 'meteor/templating';

import {Rezervacije} from '../api/rezervacije.js';

import './rezervacija.html';

Template.rezervacija.events({

});

Template.registerHelper('formatDate', function(prihod){
    return moment(prihod).format('MM-DD-YYYY');
});